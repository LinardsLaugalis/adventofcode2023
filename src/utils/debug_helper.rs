pub fn print_2d<T>(elements: &[T], variable_name: &str) where T: std::fmt::Debug {
    println!("\n{}:", variable_name);
    elements.iter().for_each(|row| println!("{:?}", row));
}