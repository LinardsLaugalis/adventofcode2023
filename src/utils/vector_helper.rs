pub fn transform_matrix_to_vector<T>(input: Vec<Vec<T>>, column: usize) -> Vec<T> {
    let mut result: Vec<T> = Vec::new();
    input.into_iter()
        .map(|e| e.into_iter().nth(column))
        .filter(|e| e.is_some())
        .for_each(|e| result.push(e.unwrap()));

    return result;
}