use std::fs::File;
use std::{io};
use std::io::BufRead;
use std::path::Path;
use std::str::FromStr;

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

pub fn read_file<T>(filename: &str, separator: char) -> Vec<Vec<T>> where T: FromStr + Default {
    let mut result: Vec<Vec<T>> = Vec::new();
    match read_lines(filename) {
        Ok(lines) => {
            for line in lines {
                result.push(split_string_by_separator(line.unwrap(), separator.to_string().as_str()));
            }
        }
        Err(error) => {
            eprintln!("Error reading file: {}", error);
        }
    }

    return result;
}

pub fn get_substring_position(line: &String, substr: String) -> usize {
    let (column_index, _) = line.as_str().match_indices(&substr).nth(0).unwrap();

    return column_index;
}

pub fn split_string_by_separator<T>(line: String, separator: &str) -> Vec<T> where T: FromStr + Default {
     return line
        .split(separator)
        .filter(|s| !s.is_empty())
        .map(|e| <T>::from_str(e))
        .map(|e| e.unwrap_or(<T>::default()))
        .collect::<Vec<T>>();
}