use crate::utils::{file_helper, vector_helper};
use std::cmp;

pub const DOT: u8 = 46;

pub fn run_code() {
    let line_matrix = file_helper::read_file::<String>("src/dec03/input", ';');
    let lines = vector_helper::transform_matrix_to_vector(line_matrix, 0);

    println!("{}", count_adjacent_numbers(lines))
}

fn count_adjacent_numbers(lines: Vec<String>) -> u128 {
    let mut result: u128 = 0;
    for (line_index, line) in lines.iter().enumerate() {
        let mut number: u128 = 0;
        for (symbol_index, symbol) in line.bytes().enumerate() {
            if !is_number_char(symbol) {
                result += add_to_result(line_index, symbol_index, number, &lines);
                number = 0;
                continue;
            }

            if is_number_char(symbol) {
                number = add_to_current_number(number, symbol);
            }
        }

        result += add_to_result(line_index, line.bytes().len(), number, &lines);

        println!("Line ended! Result: {result}");
    }

    return result;
}

fn add_to_result(
    line_i: usize,
    symbol_i: usize,
    number: u128,
    lines: &Vec<String>,
) -> u128 {
    if number == 0 {
        return 0;
    }
    println!("tries to add number: {number}");

    let start_symbol: i16 = symbol_i as i16 - ((number as f32).log10()) as i16 - 2;
    let line_len: usize = lines[0].bytes().len();

    let is_prev_symbol = start_symbol >= 0 && is_symbol(lines[line_i].bytes().nth(start_symbol as usize).unwrap());
    let is_next_symbol = symbol_i <= line_len - 1 && is_symbol(lines[line_i].bytes().nth(symbol_i).unwrap());
    if is_prev_symbol || is_next_symbol { return number; }

    let check_start = cmp::max(0, start_symbol);
    let check_end = cmp::min((symbol_i + 1) as i16, (line_len) as i16); // not inclusive
    for i in check_start..check_end {
        if line_i as i16 - 1 >= 0 && is_symbol(lines[line_i - 1].bytes().nth(i as usize).unwrap()) { return number; }
        if line_i + 1 <= lines.len() - 1 && is_symbol(lines[line_i + 1].bytes().nth(i as usize).unwrap()) { return number; }
    }

    return 0;
}

pub fn is_dot(byte_to_test: u8) -> bool {
    return byte_to_test == DOT;
}

pub fn is_number_char(byte_to_test: u8) -> bool {
    return byte_to_test >= 48 && byte_to_test < 58;
}

pub fn is_symbol(byte_to_test: u8) -> bool {
    return !is_dot(byte_to_test) && !is_number_char(byte_to_test);
}

pub fn get_number(byte_to_convert: u8) -> u8 {
    return byte_to_convert - 48;
}

pub fn add_to_current_number(number: u128, new_byte: u8) -> u128 {
    return number * 10 + get_number(new_byte) as u128;
}