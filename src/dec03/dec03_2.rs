use crate::utils::{file_helper, vector_helper};
use crate::dec03::dec03_1::{is_number_char, add_to_current_number};

pub const STAR: u8 = 42;

pub fn run_code() {
    let line_matrix = file_helper::read_file::<String>("src/dec03/input", ';');
    let lines = vector_helper::transform_matrix_to_vector(line_matrix, 0);

    println!("{}", get_gear_ratio(lines))
}

fn get_gear_ratio(lines: Vec<String>) -> u128 {
    let mut parts: Vec<Part> = Vec::new();
    let mut stars: Vec<Star> = Vec::new();

    for (line_index, line) in lines.iter().enumerate() {
        let mut number: u128 = 0;
        for (symbol_index, symbol) in line.bytes().enumerate() {
            if symbol == STAR {
                stars.push(Star { line: line_index as isize, index: symbol_index as isize });
            }

            if !is_number_char(symbol) {
                add_part(&mut parts, line_index, symbol_index, number);
                number = 0;
                continue;
            }

            if is_number_char(symbol) {
                number = add_to_current_number(number, symbol);
            }
        }

        add_part(&mut parts, line_index, line.bytes().len(), number);
    }

    return calculate_gear_ratio(&stars, &parts);
}

fn calculate_gear_ratio(stars: &Vec<Star>, parts: &Vec<Part>) -> u128 {
    let mut result: u128 = 0;
    for star in stars.iter() {
        let mut matching_parts: Vec<&Part> = Vec::new();
        for part in parts.iter() {
            let is_matching_line = star.line == part.line && (star.index == part.start_index - 1 || star.index == part.end_index + 1)
                || (star.line == part.line - 1 && star.index >= part.start_index - 1 && star.index <= part.end_index + 1)
                || (star.line == part.line + 1 && star.index >= part.start_index - 1 && star.index <= part.end_index + 1);

            if is_matching_line {
                matching_parts.push(part);
            }
        }

        if matching_parts.len() == 2 {
            let number1 = matching_parts[0].number as u128;
            let number2 = matching_parts[1].number as u128;

            result = result + (number1 * number2);
        }
    }

    return result;
}

fn print_parts(parts: &Vec<Part>) {
    for part in parts.iter() {
        println!("Part: {} {} {} {}", part.number, part.line, part.start_index, part.end_index);
    }
}

fn add_part(parts: &mut Vec<Part>, line: usize, current_symbol: usize, number: u128) {
    if number == 0 { return; }

    parts.push(Part {
        number: number as u16,
        line: line as isize,
        start_index: (current_symbol as i16 - ((number as f32).log10()) as i16 - 1) as isize,
        end_index: (current_symbol - 1) as isize,
    });
}

pub struct Part {
    pub number: u16,
    pub line: isize,
    pub start_index: isize,
    pub end_index: isize,
}

pub struct Star {
    pub line: isize,
    pub index: isize,
}