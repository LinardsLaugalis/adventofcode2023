use std::collections::HashMap;
use crate::utils::file_helper;
use debug_ignore::DebugIgnore;
use num::integer::div_ceil;

pub fn run_code() {
    let map: Vec<Vec<char>> = get_input();
    let mut visited_locations: HashMap<XY, bool> = HashMap::new();
    let mut steps: Vec<XY> = Vec::new();

    let result = move_further(Movement {
        map: DebugIgnore(&map),
        pos: get_start_position(&map),
        steps: 0,
    }, &mut visited_locations, &mut steps);

    println!("🏆 Result: {:?}", div_ceil(result.steps, 2));
}

pub fn move_further<'a>(movement: Movement<'a>, visited_locations: &'a mut HashMap<XY, bool>, all_steps: &mut Vec<XY>) -> Movement<'a> {
    all_steps.push(movement.pos.clone());
    visited_locations.insert(movement.pos, true);
    let allowed_coordinates: Vec<XY> = get_allowed_next_moves(&movement, visited_locations);
    if allowed_coordinates.len() > 0 {
        for coords in allowed_coordinates.iter() {
            return move_further(create_next_movement(movement, *coords), visited_locations, all_steps);
        }
        return movement;
    } else {
        return movement;
    }
}

fn create_next_movement(movement: Movement, coordinates: XY) -> Movement {
    let mut new_movement = movement.clone();
    new_movement.steps += 1;
    new_movement.pos = coordinates.clone();

    return new_movement;
}

fn get_allowed_next_moves(current: &Movement, visited_locations: &mut HashMap<XY, bool>) -> Vec<XY> {
    let mut results: Vec<XY> = Vec::new();
    let pos = current.pos;

    let supports_right = pos.x <= current.map[0].len() - 2
        && !['7', '|', 'J'].contains(&get_char(&current.map, pos.x, pos.y))
        && ['-', '7', 'J'].contains(&get_char(&current.map, pos.x + 1, pos.y));
    let supports_left = pos.x > 0
        && !['L', '|', 'F'].contains(&get_char(&current.map, pos.x, pos.y))
        && ['-', 'F', 'L'].contains(&get_char(&current.map, pos.x - 1, pos.y));
    let supports_up = pos.y > 0
        && !['-', '7', 'F'].contains(&get_char(&current.map, pos.x, pos.y))
        && ['|', 'F', '7'].contains(&get_char(&current.map, pos.x, pos.y - 1));
    let supports_down = pos.y <= current.map.len() - 2
        && !['-', 'L', 'J'].contains(&get_char(&current.map, pos.x, pos.y))
        && ['|', 'L', 'J'].contains(&get_char(&current.map, pos.x, pos.y + 1));

    if supports_right { results.push(XY { x: pos.x + 1, y: pos.y }); }
    if supports_left { results.push(XY { x: pos.x - 1, y: pos.y }); }
    if supports_up { results.push(XY { x: pos.x, y: pos.y - 1 }); }
    if supports_down { results.push(XY { x: pos.x, y: pos.y + 1 }); }

    let mut return_result: Vec<XY> = Vec::new();
    for result in results {
        if !visited_locations.contains_key(&result) {
            return_result.push(result);
        }
    }

    return return_result;
}

fn get_char(map: &Vec<Vec<char>>, x: usize, y: usize) -> char {
    return map[y][x];
}

pub fn get_start_position(map: &Vec<Vec<char>>) -> XY {
    for (y, line) in map.iter().enumerate() {
        for (x, char) in line.iter().enumerate() {
            if *char == 'S' { return XY { x, y }; }
        }
    }

    panic!("No coordinates found!");
}

pub fn get_input() -> Vec<Vec<char>> {
    let map: Vec<Vec<String>> = file_helper::read_file::<String>("src/dec10/input", '*');
    let mut result: Vec<Vec<char>> = Vec::new();
    for line in map.iter() {
        let mut line_chars: Vec<char> = Vec::new();
        for char in line[0].chars() {
            line_chars.push(char.clone());
        }
        result.push(line_chars);
    }

    return result;
}

#[derive(Clone, Copy, Debug)]
#[derive(Eq, Hash, PartialEq)]
pub struct XY {
    pub x: usize,
    pub y: usize,
}

#[derive(Clone, Debug)]
pub struct Movement<'a> {
    pub pos: XY,
    pub map: DebugIgnore<&'a Vec<Vec<char>>>,
    pub steps: u32,
}