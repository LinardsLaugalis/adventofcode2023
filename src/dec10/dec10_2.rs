use std::collections::HashMap;
use debug_ignore::DebugIgnore;
use crate::dec10::dec10_1::{get_input, get_start_position, move_further, Movement, XY};

const _EPS: f64 = 0.00001;
const _MIN: f64 = f64::MIN_POSITIVE;
const _MAX: f64 = f64::MAX;

pub fn run_code() {
    let map: Vec<Vec<char>> = get_input();
    let mut visited_locations: HashMap<XY, bool> = HashMap::new();
    let mut steps: Vec<XY> = Vec::new();

    move_further(Movement {
        map: DebugIgnore(&map),
        pos: get_start_position(&map),
        steps: 0,
    }, &mut visited_locations, &mut steps);

    let edges = combine_steps(steps);
    let polygon: Polygon = Polygon{edges };
    let mut tiles_inside: u64 = 0u64;
    for (y, line) in map.iter().enumerate() {
        for (x, _symbol) in line.iter().enumerate() {
            if visited_locations.contains_key(&(XY { x, y })) {
                continue;
            }

            let point = Point{x:x as f64,y: y as f64};
            if pt_in_polygon(&point, &polygon) {
                tiles_inside += 1;
            }
        }
    }

    println!("🏆 Result: {:?}", tiles_inside);
}

fn pt_in_polygon(pt: &Point, poly: &Polygon) -> bool {
    let count = poly.edges
        .iter()
        .filter(|edge| ray_intersect_seg(pt, edge))
        .count();

    count % 2 == 1
}

fn ray_intersect_seg(p: &Point, edge: &Edge) -> bool {
    let mut pt = p.clone();
    let (mut a, mut b): (&Point, &Point) = (&edge.pt1, &edge.pt2);
    if a.y > b.y {
        std::mem::swap(&mut a, &mut b);
    }
    if pt.y == a.y || pt.y == b.y {
        pt.y += _EPS;
    }

    if (pt.y > b.y || pt.y < a.y) || pt.x > a.x.max(b.x) { return false }
    else if pt.x < a.x.min(b.x) { true }
    else {
        let m_red = if (a.x - b.x).abs() > _MIN { (b.y - a.y) / (b.x - a.x) } else { _MAX };
        let m_blue = if (a.x - pt.x).abs() > _MIN { (pt.y - a.y) / (pt.x - a.x) } else { _MAX };

        m_blue >= m_red
    }
}

fn combine_steps(steps: Vec<XY>) -> Vec<Edge> {
    let mut edges: Vec<Edge> = Vec::new();

    let mut start_point = steps[0];
    let mut prev_diff_x = (steps[1].x - steps[0].x) as i32;
    let mut prev_diff_y = (steps[1].y - steps[1].y) as i32;

    for (index, step) in steps[1..].iter().enumerate() {
        let pos = 1 + index;

        let diff_x = step.x as i32 - steps[pos - 1].x as i32;
        let diff_y = step.y as i32 - steps[pos - 1].y as i32;

        if prev_diff_x != diff_x || prev_diff_y != diff_y {

            edges.push(Edge{pt1: to_point(start_point), pt2: to_point(steps[pos-1])});
            start_point = steps[pos - 1];
            prev_diff_x = diff_x;
            prev_diff_y = diff_y;
        }
    }

    edges.push(Edge{pt1: to_point(start_point), pt2: to_point(steps[0])});

    return edges;
}

fn to_point(xy: XY)->Point {
    return Point {
        x: xy.x as f64,
        y: xy.y as f64
    }
}

#[derive(Clone, Debug)]
struct Point {
    x: f64,
    y: f64,
}

#[derive(Clone, Debug)]
struct Edge {
    pt1: Point,
    pt2: Point,
}

impl Edge {
    fn new(pt1: (f64, f64), pt2: (f64, f64)) -> Edge {
        Edge {
            pt1: Point { x: pt1.0, y: pt1.1 },
            pt2: Point { x: pt2.0, y: pt2.1 },
        }
    }
}

struct Polygon {
    edges: Vec<Edge>
}