use crate::utils::{file_helper, vector_helper};

pub fn run_code() {
    let cards = get_cards();
    let mut points:u128 = 0;
    for card in cards.iter() {
        points = points + get_points(card);
    }

    println!("{}", points);
}

pub fn get_cards() -> Vec<Card> {
    let line_matrix = file_helper::read_file::<String>("src/dec04/input", '*');
    let lines = vector_helper::transform_matrix_to_vector(line_matrix, 0);

    let mut cards: Vec<Card> = Vec::new();
    for line in lines.iter() {
        cards.push(get_card(line));
    }

    return cards;
}

pub fn get_matching_number_count(card: &Card)->u32 {
    let mut matches = 0;
    for winning_number in card.winning_numbers.iter() {
        let mut match_found = false;
        for number in card.numbers.iter() {
            if winning_number == number {
                matches = matches + 1;
                match_found = true;
                break;
            }
        }

        if match_found {continue;}

    }

    return matches;
}

fn get_points(card: &Card) -> u128 {
    let matches = get_matching_number_count(card);
    if matches == 0 { return 0; }

    let base: u128 = 2;
    return base.pow(matches-1);
}

fn get_card(line: &String) -> Card {

    let column_position = file_helper::get_substring_position(&line, String::from(":"));
    let bar_position = file_helper::get_substring_position(&line, String::from("|"));

    return Card {
        id: line[5..column_position].replace(" ", "").parse::<u16>().unwrap(),
        winning_numbers: get_numbers_from_string(line[column_position + 2..bar_position -1].to_string()),
        numbers: get_numbers_from_string(line[bar_position + 2..].to_string()),
        copies: 0
    };
}

fn get_numbers_from_string(line: String) -> Vec<u16> {
    return file_helper::split_string_by_separator(line, " ");
}

#[derive(Clone)]
pub struct Card {
    pub id: u16,
    pub winning_numbers: Vec<u16>,
    pub numbers: Vec<u16>,
    pub copies: u128,
}