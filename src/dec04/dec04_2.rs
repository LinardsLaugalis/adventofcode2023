use std::collections::HashMap;
use crate::dec04::dec04_1::{get_cards, get_matching_number_count};

pub fn run_code() {
    let all_cards = get_cards();
    let mut copies: HashMap<u16, u32> = HashMap::new();

    for card in all_cards.iter() {
        let index = card.id + 1;
        let matching_numbers = get_matching_number_count(&card);
        let card_copies = get_card_copy_count(&copies, &card.id);
        for _i in 0..card_copies + 1 {
            for j in index ..index + (matching_numbers as u16) {
                let current_copies = get_card_copy_count(&copies, &j);
                copies.insert(j, current_copies + 1);
            }
        }
    }

    let mut result: u64 = all_cards.len() as u64;
    for (_card_id, copy_count) in copies.iter() {
        result = result + (*copy_count as u64);
    }

    println!("{result}");
}

fn get_card_copy_count(copies: &HashMap<u16, u32>, i: &u16) -> u32 {
    let mut current_copies: u32 = 0;
    if copies.contains_key(&i) {
        current_copies = *copies.get(&i).unwrap();
    }
    current_copies
}