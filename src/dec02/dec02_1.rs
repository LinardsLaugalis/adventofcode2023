use crate::utils::{file_helper, vector_helper};

pub fn run_code() {
    let line_matrix = file_helper::read_file::<String>("src/dec02/input", '*');
    let lines: Vec<String> = vector_helper::transform_matrix_to_vector(line_matrix, 0);
    let games: Vec<Game> = lines.iter().map(|l| get_game(l)).collect();

    let mut result: u16 = 0;
    games.iter().for_each(|g| result += get_possible_game_id(g));

    println!("{}", result);
}

fn get_possible_game_id(game: &Game) -> u16 {
    for set in &game.sets {
        if set.red > 12 || set.green > 13 || set.blue > 14 {
            return 0;
        }
    }

    return game.id;
}

pub fn get_game(line: &String) -> Game {
    let column_position = get_substring_position(line, String::from(":"));

    let game = Game {
        id: line[5..column_position].to_string().parse::<u16>().unwrap(),
        sets: line[column_position + 1..].split(";").map(|e| get_set(e)).collect(),
    };

    return game;
}

fn get_set(set_str: &str) -> Set {
    let colors: Vec<_> = set_str.split(",").collect();
    let mut set = Set {
        red: 0,
        green: 0,
        blue: 0,
    };

    for color in &colors {
        let color_str = &color[1..];
        let space_index = get_substring_position(&color_str.to_string(), String::from(" "));
        let count = &color_str[..space_index].parse::<u16>().unwrap();
        let color_name = &color_str[space_index + 1..];

        match color_name {
            "red" => { set.red = *count; }
            "green" => { set.green = *count; }
            "blue" => { set.blue = *count; }
            _ => {}
        }
    }

    return set;
}

fn get_substring_position(line: &String, substr: String) -> usize {
    let (column_index, _) = line.as_str().match_indices(&substr).nth(0).unwrap();

    return column_index;
}

pub struct Game {
    pub id: u16,
    pub sets: Vec<Set>,
}

pub struct Set {
    pub red: u16,
    pub green: u16,
    pub blue: u16,
}
