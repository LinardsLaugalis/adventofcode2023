use crate::utils::{file_helper, vector_helper};
use crate::dec02::dec02_1::{Game, get_game};

pub fn run_code() {
    let line_matrix = file_helper::read_file::<String>("src/dec02/input", '*');
    let lines: Vec<String> = vector_helper::transform_matrix_to_vector(line_matrix, 0);
    let games: Vec<Game> = lines.iter().map(|l| get_game(l)).collect();

    let mut result: u128 = 0;
    games.iter().for_each(|g| result += get_game_power(g) as u128);

    println!("{}", result);
}

fn get_game_power(game: &Game) -> u16 {
    let mut min_red: u16 = 0;
    let mut min_blue: u16 = 0;
    let mut min_green: u16 = 0;

    for set in &game.sets {
        if set.red > min_red {min_red = set.red}
        if set.blue > min_blue {min_blue = set.blue}
        if set.green > min_green {min_green = set.green}
    }

    return min_red * min_blue * min_green;
}