use num::abs;
use crate::utils::file_helper;
use crate::utils::vector_helper::transform_matrix_to_vector;

pub fn run_code() {
    let debug = true;
    let galaxies = get_galaxies();

    let mut result: u64 = 0;
    for (current, galaxy) in galaxies.iter().enumerate() {
        for next in current + 1..galaxies.len() {
            let distance = (abs(galaxy.x - galaxies[next].x) + abs(galaxy.y - galaxies[next].y)) as u64;
            if debug { println!("current {:?}, next: {:?}, dist: {distance}", galaxy, galaxies[next]); }
            result += distance;
        }
    }

    if debug { println!("{:?}", galaxies); }
    println!("🏆 Result: {result}");
}

pub fn get_galaxies() -> Vec<Point> {
    let lines = transform_matrix_to_vector(
        file_helper::read_file::<String>("src/dec11/input", ' '),
        0,
    );

    let mut expanded_matrix: Vec<String> = lines.clone();

    let mut inserted_rows = 0;
    for (y, line) in lines.iter().enumerate() {
        if is_empty_row(&line) {
            expanded_matrix.insert(y + inserted_rows, line.clone());
            inserted_rows += 1;
        }
    }

    let mut inserted_cols = 0;
    for (x, _char) in lines[0].chars().enumerate() {
        if is_empty_col(x, &lines) {
            for row in 0..lines.len() + inserted_rows {
                expanded_matrix[row].insert(x + inserted_cols, '.');
            }
            inserted_cols = inserted_cols + 1;
        }
    }

    return get_galaxies_from_matrix(&expanded_matrix);
}

pub fn get_galaxies_from_matrix(matrix: &Vec<String>) -> Vec<Point> {
    let mut galaxies: Vec<Point> = Vec::new();
    for (y, line) in matrix.iter().enumerate() {
        for (x, char) in line.chars().enumerate() {
            if char == '#' { galaxies.push(Point { x: x as i32, y: y as i32 }) }
        }
    }

    return galaxies;
}

pub fn is_empty_row(line: &String) -> bool {
    for char in line.chars() {
        if char != '.' { return false; }
    }

    return true;
}

pub fn is_empty_col(x: usize, map: &Vec<String>) -> bool {
    for (index, _line) in map.iter().enumerate() {
        if map[index].chars().nth(x).unwrap() != '.' { return false; }
    }

    return true;
}

#[derive(Clone, Copy, Debug)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}