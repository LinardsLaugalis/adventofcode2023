use std::mem::swap;
use num::abs;
use crate::dec11::dec11_1::{get_galaxies_from_matrix, is_empty_col, is_empty_row, Point};
use crate::utils::file_helper;
use crate::utils::vector_helper::transform_matrix_to_vector;

pub const EXPANSION_FACTOR: u128 = 1000000;

pub fn run_code() {
    let lines = transform_matrix_to_vector(
        file_helper::read_file::<String>("src/dec11/input", ' '),
        0,
    );

    let inserted_rows = get_inserted_rows(&lines);
    let inserted_columns = get_inserted_columns(&lines);

    let galaxies = get_galaxies_from_matrix(&lines);

    let result: u128 = get_galaxy_distances(&galaxies, &inserted_rows, &inserted_columns);
    println!("🏆 Result: {result}");
}

fn get_galaxy_distances(galaxies: &Vec<Point>, ins_rows: &Vec<usize>, ins_columns: &Vec<usize>) -> u128 {
    let mut result = 0u128;
    for (current, galaxy) in galaxies.iter().enumerate() {
        for next in current + 1..galaxies.len() {
            let rows_between = get_inserted_between(galaxy.y, galaxies[next].y, ins_rows);
            let cols_between = get_inserted_between(galaxy.x, galaxies[next].x, ins_columns);
            let distance = (abs(galaxy.x - galaxies[next].x)
                + abs(galaxy.y - galaxies[next].y)) as u128
                + rows_between
                + cols_between;

            result += distance;
        }
    }

    return result;
}

fn get_inserted_between(current_val: i32, next_val: i32, ins_values: &Vec<usize>) -> u128 {
    let mut current = current_val;
    let mut next = next_val;

    if next < current {
        swap(&mut next, &mut current);
    }

    let mut count = 0u128;
    for inserted in ins_values.iter() {
        let inserted_casted = *inserted as i32;
        if current < inserted_casted && inserted_casted < next {
            count += 1;
        }
    }

    if count == 0 {return 0;}

    return count * EXPANSION_FACTOR - count;
}

fn get_inserted_columns(lines: &Vec<String>) -> Vec<usize> {
    let mut inserted_cols: Vec<usize> = Vec::new();
    for (x, _char) in lines[0].chars().enumerate() {
        if is_empty_col(x, &lines) {
            inserted_cols .push(x);
        }
    }

    return inserted_cols;
}

fn get_inserted_rows(lines: &Vec<String>) -> Vec<usize> {
    let mut inserted_rows: Vec<usize> = Vec::new();
    for (y, line) in lines.iter().enumerate() {
        if is_empty_row(&line) {
            inserted_rows.push(y);
        }
    }

    return inserted_rows;
}