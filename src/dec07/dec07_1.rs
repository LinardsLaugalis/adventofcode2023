use std::cmp::Ordering;
use std::collections::{HashMap};
use std::str::FromStr;
use crate::utils::{file_helper};

pub fn run_code() {
    let mut hands: Vec<Hand> = get_cards();
    for hand in hands.iter_mut() {
        set_hand_type(hand, false);
    }

    sort_hands_desc(&mut hands, get_char_strength);
    calculate_result(&mut hands);
}

pub fn calculate_result(hands: &mut Vec<Hand>) {
    let mut result: u128 = 0;
    for (index, hand) in hands.iter_mut().rev().enumerate() {
        hand.rank = index as u16 + 1u16;
        println!("{:?}", hand);
        result += (hand.bid * hand.rank as u32) as u128;
    }

    println!("🏆 Result: {result}");
}

pub fn sort_hands_desc(hands: &mut Vec<Hand>, get_char_strength: fn(&char) -> u8) {
    hands.sort_by(|a, b| {
        let mut cmp = b.hand_type.cmp(&a.hand_type);
        if cmp != Ordering::Equal {
            return cmp;
        }

        for char_index in 0..5 {
            let a_char = a.cards.chars().nth(char_index).unwrap();
            let b_char = b.cards.chars().nth(char_index).unwrap();

            let a_strength = get_char_strength(&a_char);
            let b_strength = get_char_strength(&b_char);

            cmp = b_strength.cmp(&a_strength);
            if cmp != Ordering::Equal {
                return cmp;
            }
        }

        return b.rank.cmp(&a.rank);
    });
}

pub fn set_hand_type(hand: &mut Hand, with_jokers: bool) {
    let mut char_map: HashMap<char, u8> = HashMap::new();
    for char in hand.cards.chars() {
        let current_val = char_map.get(&char);
        char_map.insert(
            char,
            match current_val {
                Some(value) => { *value + 1u8 }
                _ => { 1 }
            },
        );
    }


    let joker_count: u8 = *match with_jokers {
        false => { &0 }
        true => char_map.get(&'J').unwrap_or(&0)
    };

    if joker_count > 3 {
        hand.hand_type = 6; return;
    }

    if with_jokers && char_map.contains_key(&'J') {
        char_map.remove(&'J');
    }

    let mut freq: Vec<u8> = char_map.iter().map(|(_, v)| *v).collect();
    freq.sort_by(|a, b| b.cmp(a));
    println!("{} - {:?} - {:?}", hand.cards, char_map, freq);


    if freq[0] + joker_count >= 5 { hand.hand_type = 6; return;}
    if freq[0] + joker_count >= 4 { hand.hand_type = 5; return;}
    if freq[0] + freq[1] + joker_count >=5 { hand.hand_type = 4; return;}
    if freq[0] + joker_count >=3 {hand.hand_type = 3; return;}
    if freq[0] + freq[1] + joker_count >=4 {hand.hand_type = 2; return;}
    if freq[0] + joker_count >=2 { hand.hand_type = 1; return;}
}

fn get_char_strength(char: &char) -> u8 {
    return match *char {
        'A' => { 13 }
        'K' => { 12 }
        'Q' => { 11 }
        'J' => { 10 }
        'T' => { 9 }
        _ => { u8::from_str(char.to_string().as_str()).unwrap() - 1 }
    };
}

pub fn get_cards() -> Vec<Hand> {
    let mut hands: Vec<Hand> = Vec::new();
    let line_matrix = file_helper::read_file::<String>("src/dec07/input", ' ');
    for line in line_matrix {
        hands.push(Hand {
            cards: line[0].clone(),
            bid: u32::from_str(line[1].as_str()).unwrap(),
            rank: u16::MAX,
            hand_type: 0,
        });
    }

    return hands;
}

#[derive(Clone, Debug)]
pub struct Hand {
    pub cards: String,
    pub hand_type: u8,
    pub bid: u32,
    pub rank: u16,
}