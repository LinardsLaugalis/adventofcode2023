use std::str::FromStr;
use crate::dec07::dec07_1::{Hand, get_cards, sort_hands_desc, set_hand_type, calculate_result};

pub fn run_code() {
    let mut hands: Vec<Hand> = get_cards();
    for hand in hands.iter_mut() {
        set_hand_type(hand, true);
    }

    sort_hands_desc(&mut hands, get_char_strength);
    calculate_result(&mut hands);
}

fn get_char_strength(char: &char)-> u8 {
    return match *char {
        'A' => {13},
        'K' => {12},
        'Q' => {11},
        'J' => {0},
        'T' => {9},
        _ => {u8::from_str(char.to_string().as_str()).unwrap() -1 }
    }
}