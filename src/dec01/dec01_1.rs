use crate::utils::{file_helper, vector_helper};

pub fn run_code() {
    let line_matrix = file_helper::read_file::<String>("src/dec01/input", ' ');
    let lines = vector_helper::transform_matrix_to_vector(line_matrix, 0);
    let numbers: Vec<u8> = lines.into_iter().map(|e| get_decoded_number(e)).collect();


    println!("{:?}", get_sum_of_numbers(numbers));
}

fn get_decoded_number(line: String) -> u8 {
    let mut first_number: u8 = u8::MAX;
    let mut last_number: u8 = 0;

    for byte in line.bytes() {
        if byte >= 48 && byte <= 57 {
            last_number = byte - 48;
            if first_number == u8::MAX {
                first_number = last_number;
            }
        }
    }

    return first_number * 10 + last_number;
}

pub fn get_sum_of_numbers(numbers: Vec<u8>)-> u64 {
    let mut result: u64 = 0;
    numbers.iter().for_each(|num| result += *num as u64);

    return result;
}