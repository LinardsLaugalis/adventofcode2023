use std::collections::HashMap;
use crate::utils::{file_helper, vector_helper};
use crate::dec01::dec01_1::{get_sum_of_numbers};

pub fn run_code() {
    let line_matrix = file_helper::read_file::<String>("src/dec01/input", ' ');
    let lines = vector_helper::transform_matrix_to_vector(line_matrix, 0);
    let numbers: Vec<u8> = lines.into_iter().map(|e| get_decoded_number(e)).collect();

    println!("{:?}", get_sum_of_numbers(numbers));
}

fn get_decoded_number(original_line: String) -> u8 {
    let first: u8 = get_first_number(
        get_replaced_string(original_line.clone(), get_covert_map()),
    );

    let last: u8 = get_first_number(
        get_replaced_string(
            original_line.clone().chars().rev().collect::<String>(),
            get_covert_map_reverse(),
        )
    );

    return first * 10 + last;
}

fn get_first_number(new_line: String) -> u8 {
    for byte in new_line.clone().bytes() {
        if byte >= 48 && byte <= 57 {
            return byte - 48;
        }
    }

    return 0;
}

fn get_replaced_string(original_line: String, convert_map: HashMap<char, Vec<&'static str>>) -> String {
    let mut new_line = original_line.clone();

    loop {
        let line = new_line.clone();

        let mut was_replaced = false;
        for (i, byte) in line.bytes().enumerate() {
            let possible_numbers = convert_map.get(&(byte.to_ascii_lowercase() as char));
            if !possible_numbers.is_some() {
                continue;
            }

            for number in possible_numbers.unwrap() {
                if line[i..].starts_with(number) {
                    let char = (get_char_value(number.to_string()) + 48) as char;
                    new_line = line.replace(number, char.to_string().as_str());
                    was_replaced = true;
                    break;
                }
            }

            if was_replaced {
                break;
            }
        }

        if !was_replaced {
            break;
        }
    }

    return new_line;
}


fn get_covert_map() -> HashMap<char, Vec<&'static str>> {
    let mut convert_map: HashMap<char, Vec<&str>> = HashMap::new();
    convert_map.insert('o', vec!["one"]);
    convert_map.insert('t', vec!["two", "three"]);
    convert_map.insert('f', vec!["four", "five"]);
    convert_map.insert('s', vec!["six", "seven"]);
    convert_map.insert('e', vec!["eight"]);
    convert_map.insert('n', vec!["nine"]);

    return convert_map;
}

fn get_covert_map_reverse() -> HashMap<char, Vec<&'static str>> {
    let mut convert_map: HashMap<char, Vec<&str>> = HashMap::new();
    convert_map.insert('e', vec!["eno", "eerht", "evif", "enin"]);
    convert_map.insert('o', vec!["owt"]);
    convert_map.insert('r', vec!["ruof"]);
    convert_map.insert('x', vec!["xis"]);
    convert_map.insert('t', vec!["thgie"]);
    convert_map.insert('n', vec!["neves"]);

    return convert_map;
}

fn get_char_value(number: String) -> u8 {
    return match number.as_str() {
        "one" | "eno" => { 1 }
        "two" | "owt" => { 2 }
        "three" | "eerht" => { 3 }
        "four" | "ruof" => { 4 }
        "five" | "evif" => { 5 }
        "six" | "xis" => { 6 }
        "seven" | "neves" => { 7 }
        "eight" | "thgie" => { 8 }
        "nine" | "enin"=> { 9 }
        _ => { 0 }
    };
}