use crate::utils::file_helper;

pub fn run_code() {
    let number_lines: Vec<Vec<i32>> = file_helper::read_file::<i32>("src/dec09/input", ' ');
    println!("🏆 Result: {}", process_number_lines(&number_lines, get_result));
}

pub fn process_number_lines(number_lines: &Vec<Vec<i32>>, result_method: fn(&mut Vec<Vec<i32>>) -> i128) -> i128 {
    let mut result: i128 = 0;
    for number_line in number_lines {
        result = result + process_line(&number_line, result_method);
    }

    return result;
}

fn process_line(line: &Vec<i32>, result_method: fn(&mut Vec<Vec<i32>>) -> i128) -> i128 {
    let mut diff_map: Vec<Vec<i32>> = get_full_diff_map(line);
    return result_method(&mut diff_map);
}

fn get_result(diff_map: &mut Vec<Vec<i32>>) -> i128 {
    let mut last_map_value:i32 = 0;
    for map in diff_map.iter_mut().rev() {
        let last_value = *map.last().unwrap();
        let new_value = last_value + last_map_value;
        map.push(new_value);
        last_map_value = new_value;
    }

    return last_map_value as i128;
}

fn get_full_diff_map(line: &Vec<i32>) -> Vec<Vec<i32>> {
    let mut diff_map: Vec<Vec<i32>> = Vec::new();
    diff_map.push(line.clone());
    let mut line_to_process = line.clone();
    loop {
        let mut current_diff: Vec<i32> = Vec::new();
        for (index, number) in line_to_process.iter().enumerate() {
            if index == 0 {
                continue;
            }

            current_diff.push(number - line_to_process[index - 1]);
        }

        diff_map.push(current_diff.clone());
        if is_diff_completed(&current_diff) { break; }
        line_to_process = current_diff.clone();
    }

    return diff_map;
}

fn is_diff_completed(diff: &Vec<i32>) -> bool {
    for number in diff.iter() {
        if *number != 0 {
            return false;
        }
    }

    return true;
}
