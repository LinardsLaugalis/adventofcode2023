use crate::dec09::dec09_1::process_number_lines;
use crate::utils::file_helper;

pub fn run_code() {
    let number_lines: Vec<Vec<i32>> = file_helper::read_file::<i32>("src/dec09/input", ' ');
    println!("🏆 Result: {}", process_number_lines(&number_lines, get_result_part_2));
}

fn get_result_part_2(diff_map: &mut Vec<Vec<i32>>) -> i128 {
    let mut last_map_value:i32 = 0;
    for map in diff_map.iter_mut().rev() {
        let first_value = *map.first().unwrap();
        let new_value = first_value - last_map_value;
        map.splice(..0, vec![new_value].drain(..));
        last_map_value = new_value;
    }

    return last_map_value as i128;
}