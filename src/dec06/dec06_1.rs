use crate::utils::{file_helper};

pub fn run_code() {
    let mut races = get_input();
    calculate_possibilities(&mut races);
}

pub fn calculate_possibilities(races: &mut Vec<Race>) {
    let mut result: u128 = u128::MAX;
    for race in races.iter_mut() {
        for time in 1..race.time {
            let calc_distance = (time) * (race.time - time);
            if calc_distance > race.distance {
                race.possibilities = race.possibilities + 1;
            }
        }
    }

    for race in races {
        if result == u128::MAX && race.possibilities > 0 {
            result = 1;
        }

        result = result * (race.possibilities as u128);
    }

    println!("Result: {result}");
}

pub fn get_input() -> Vec<Race> {
    let mut races: Vec<Race> = Vec::new();
    let line_matrix = file_helper::read_file::<u32>("src/dec06/input", ' ');
    for index in 1..line_matrix[0].len() {
        races.push(Race {
            id: (index as u32) - 1,
            time: line_matrix[0][index] as u128,
            distance: line_matrix[1][index] as u128,
            possibilities: 0,
        });
    }

    return races;
}

#[derive(Clone, Copy, Debug)]
pub struct Race {
    pub id: u32,
    pub time: u128,
    pub distance: u128,
    pub possibilities: u128,
}