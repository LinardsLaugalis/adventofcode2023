use crate::utils::{file_helper};
use crate::dec06::dec06_1::{Race, calculate_possibilities};
use crate::utils::vector_helper::transform_matrix_to_vector;

pub fn run_code() {
    let mut races = get_input();
    calculate_possibilities(&mut races);
}

pub fn get_input() -> Vec<Race> {
    let mut races: Vec<Race> = Vec::new();
    let line_matrix = file_helper::read_file::<String>("src/dec06/input", ':');
    let lines: Vec<u128> = transform_matrix_to_vector(line_matrix, 1)
        .iter_mut()
        .map(|l| l.replace(" ", "").to_string())
        .map(|s| s.parse::<u128>().unwrap())
        .collect();

    races.push(Race { id: 0, time: lines[0], distance: lines[1], possibilities: 0 });

    return races;
}
