use std::u64::MAX;
use crate::utils::{file_helper, vector_helper};

pub const TYPE_LOCATION: u8 = 7;

pub fn run_code() {
    let lines = get_input();
    let mut seeds: Vec<Seed> = get_seeds(&lines);
    let mut mappings: Vec<GardeningMapping> = get_mappings(&lines);
    perform_mapping(&mut seeds, &mut mappings);
}

pub fn perform_mapping(mut seeds: &mut Vec<Seed>, mappings: &mut Vec<GardeningMapping>) {
    let mut previous_type: u8 = mappings[0].map_from;
    for mapping in mappings.iter(){
        if previous_type != mapping.map_from {
            set_source_range(&mut seeds, mapping.map_from as usize);
            previous_type = mapping.map_from;
        }

        for seed in seeds.iter_mut() {
            apply_affected_mappings(seed, mapping);
        }
    }
    set_source_range(&mut seeds, (previous_type + 1) as usize);

    let mut min_location: u64 = MAX;
    for seed in seeds {
        if seed.mappings[TYPE_LOCATION as usize] < min_location {
            min_location = seed.mappings[TYPE_LOCATION as usize];
        }
    }

    println!("{}", min_location);
}


fn set_source_range(seeds: &mut Vec<Seed>, current_type: usize) {
    for seed in seeds.iter_mut() {
        if seed.mappings[current_type] == MAX {
            seed.mappings[current_type] = seed.mappings[current_type - 1];
            seed.mapping_applied = current_type as u8;
        }
    }
}

fn apply_affected_mappings(seed: &mut Seed, mapping: &GardeningMapping) {
    let current_value = seed.mappings[mapping.map_from as usize];
    let range_end = mapping.source_range_start + mapping.length;

    if current_value >= mapping.source_range_start && current_value <= range_end {
        if seed.mapping_applied == mapping.map_to {return;}
        let offset: i64 = mapping.dest_range_start as i64 - mapping.source_range_start as i64;
        seed.mappings[mapping.map_to as usize] = (current_value as i64 + offset) as u64;
        seed.mapping_applied = mapping.map_to;
    }
}

pub fn get_input() -> Vec<String> {
    let line_matrix = file_helper::read_file::<String>("src/dec05/input", '*');
    let lines = vector_helper::transform_matrix_to_vector(line_matrix, 0);

    return lines;
}

pub fn get_seeds(lines: &Vec<String>) -> Vec<Seed> {
    let mut seeds: Vec<Seed> = Vec::new();
    let col_position = file_helper::get_substring_position(&lines[0].clone(), String::from(":"));
    let seed_ids: Vec<u64> = file_helper::split_string_by_separator(lines[0][col_position + 1..].to_string(), " ");
    for id in seed_ids {
        seeds.push(create_seed(id));
    }

    return seeds;
}


pub fn get_mappings(lines: &Vec<String>) -> Vec<GardeningMapping> {
    let mut map_from: u8 = 0;
    let mut map_to: u8 = 0;

    let mut mappings: Vec<GardeningMapping> = Vec::new();
    for line in lines[1..].iter() {
        if line.contains("map:") {
            (map_from, map_to) = get_types(line.as_str());
            continue;
        }

        let numbers: Vec<u64> = file_helper::split_string_by_separator(line.clone(), " ");

        mappings.push(GardeningMapping {
            map_from,
            map_to,
            dest_range_start: numbers[0],
            source_range_start: numbers[1],
            length: numbers[2],
        });
    }
    mappings
}


pub fn create_seed(id: u64) -> Seed {
    return Seed { id, mapping_applied: 0, mappings: [id, MAX, MAX, MAX, MAX, MAX, MAX, MAX] };
}

pub fn get_types(line: &str) -> (u8, u8) {
    return match line {
        "seed-to-soil map:" => { (0u8, 1u8) }
        "soil-to-fertilizer map:" => { (1u8, 2u8) }
        "fertilizer-to-water map:" => { (2u8, 3u8) }
        "water-to-light map:" => { (3u8, 4u8) }
        "light-to-temperature map:" => { (4u8, 5u8) }
        "temperature-to-humidity map:" => { (5u8, 6u8) }
        "humidity-to-location map:" => { (6u8, 7u8) }
        _ => { (0u8, 0u8) }
    };
}

#[derive(Clone, Debug)]
pub struct GardeningMapping {
    pub map_from: u8,
    pub map_to: u8,
    pub dest_range_start: u64,
    pub source_range_start: u64,
    pub length: u64,
}

#[derive(Clone, Debug)]
pub struct Seed {
    id: u64,
    mappings: [u64; 8],
    mapping_applied: u8
}

