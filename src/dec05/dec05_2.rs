use crate::utils::{file_helper};
use crate::dec05::dec05_1::{get_mappings, GardeningMapping, get_input};

pub fn run_code() {
    let lines = get_input();
    let mut seeds: Vec<SeedInterval> = get_seeds_by_intervals(&lines);
    let mut mappings: Vec<GardeningMapping> = get_mappings(&lines);

    perform_interval_mapping(&mut seeds, &mut mappings);
}

fn perform_interval_mapping(seeds: &mut Vec<SeedInterval>, mappings: &mut Vec<GardeningMapping>) {
    let mut previous_type: u8 = mappings[0].map_from;
    for mapping in mappings.iter_mut() {
        if previous_type != mapping.map_from {
            set_source_range(seeds, mapping.map_from as usize);
            previous_type = mapping.map_from;
        }

        let mut new_seeds: Vec<SeedInterval> = Vec::new();
        for seed in seeds.iter_mut() {
            let new_seeds_returned = apply_affected_mappings(seed, mapping);
            if new_seeds_returned.is_some() {
                for new_seed in new_seeds_returned.unwrap() {
                    new_seeds.push(new_seed);
                }
            }
        }

        for seed in new_seeds {
            seeds.push(seed);
        }
    }
    set_source_range(seeds, (previous_type + 1) as usize);

    let mut min_result = u64::MAX;
    for seed in seeds {
        if seed.start < min_result {
            min_result = seed.start;
        }
    }

    println!("\n\n🏆Result: {min_result}");
}

fn apply_affected_mappings(seed: &mut SeedInterval, mapping: &mut GardeningMapping) -> Option<Vec<SeedInterval>> {
    if seed.mapping_applied == mapping.map_to { return None; }
    let mapping_src_range_end = mapping.source_range_start + mapping.length - 1;

    // Fully includes
    if seed.start >= mapping.source_range_start && seed.end <= mapping_src_range_end {
        apply_mapping(seed, seed.start as i64, seed.end as i64, mapping);
        return None;
    }

    let mut cloned_seed = seed.clone();
    let mut has_cloned: bool = false;

    if seed.start <= mapping.source_range_start && seed.end >= mapping_src_range_end {
        let mut cloned_seed_2 = seed.clone();
        cloned_seed.end = mapping.source_range_start - 1;
        cloned_seed_2.start = mapping_src_range_end + 1;

        apply_mapping(seed, mapping.source_range_start as i64, mapping_src_range_end as i64, mapping);

        let mut cloned_seeds: Vec<SeedInterval> = Vec::new();
        if (cloned_seed.end as i64 - cloned_seed.start as i64) >= 0 { cloned_seeds.push(cloned_seed); }
        if (cloned_seed_2.end as i64 - cloned_seed_2.start as i64) >= 0 { cloned_seeds.push(cloned_seed_2); }
        return Some(cloned_seeds);
    }

    if mapping_src_range_end <= seed.end && mapping.source_range_start <= seed.start && seed.start <= mapping_src_range_end {
        apply_mapping(seed, seed.start as i64, mapping_src_range_end as i64, mapping);

        let size_diff = cloned_seed.start as i64 - mapping_src_range_end as i64;
        cloned_seed.start = (cloned_seed.start as i64 - size_diff + 1) as u64;
        has_cloned = true;
    }

    if seed.start <= mapping.source_range_start && seed.end <= mapping_src_range_end && mapping.source_range_start <= seed.end {

        let size_diff = mapping.source_range_start as i64 - seed.start as i64;
        cloned_seed.end = (seed.start as i64 + size_diff - 1) as u64;
        apply_mapping(seed, mapping.source_range_start as i64, seed.end as i64, mapping);
        has_cloned = true;
    }

    return if has_cloned { Some(vec![cloned_seed]) } else { None };
}

fn apply_mapping(seed: &mut SeedInterval, start: i64, end: i64, mapping: &GardeningMapping) {
    let mapping_offset: i64 = mapping.dest_range_start as i64 - mapping.source_range_start as i64;
    seed.mapping_applied = mapping.map_to;
    seed.history.push((seed.start, seed.end));

    seed.start = (start + mapping_offset) as u64;
    seed.end = (end + mapping_offset) as u64;
}

fn set_source_range(seeds: &mut Vec<SeedInterval>, current_type: usize) {
    for seed in seeds.iter_mut() {
        if seed.mapping_applied == (current_type - 1) as u8 {
            seed.history.push((seed.start, seed.end));
            seed.mapping_applied = current_type as u8;
        }
    }
}

fn get_seeds_by_intervals(lines: &Vec<String>) -> Vec<SeedInterval> {
    let mut seeds: Vec<SeedInterval> = Vec::new();
    let col_position = file_helper::get_substring_position(&lines[0].clone(), String::from(":"));
    let numbers: Vec<u64> = file_helper::split_string_by_separator(lines[0][col_position + 1..].to_string(), " ");

    let mut seed_from: u64 = numbers[0];
    for (index, number) in numbers.iter().enumerate() {
        if index % 2 == 0 {
            seed_from = *number;
            continue;
        }

        let mut offsets = [i64::MAX; 8];
        offsets[0] = 0;

        seeds.push(SeedInterval {
            start: seed_from,
            end: seed_from + number - 1,
            history: Vec::new(),
            mapping_applied: 0,
        });
    }

    return seeds;
}

#[derive(Clone, Debug)]
struct SeedInterval {
    start: u64,
    end: u64,
    history: Vec<(u64, u64)>,
    mapping_applied: u8,
}
