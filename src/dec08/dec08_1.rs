use std::collections::{HashMap};
use crate::utils::{file_helper};

pub fn run_code() {
    let lines: Vec<Vec<String>> = get_input_lines();
    let pattern: String = lines[0][0].clone();
    let hash_map = get_map(lines);

    println!("🏆 Result: {}", follow_map(pattern, hash_map));
}

pub fn follow_map(pattern: String, map: HashMap<String, [String; 2]>) -> u128 {
    let mut result = 0u128;
    let mut current_position = "AAA".to_string();
    loop {
        for direction in pattern.chars() {
            let index = if direction == 'R' { 1usize } else { 0usize};
            result = result + 1;
            let values = map.get(&current_position).unwrap().clone();
            current_position = values[index].clone();

            if current_position == "ZZZ" {
                return result;
            }
        }
    }
}

pub fn get_input_lines() -> Vec<Vec<String>> {
    return file_helper::read_file::<String>("src/dec08/input", '=');
}

pub fn get_map(lines: Vec<Vec<String>>) -> HashMap<String, [String; 2]> {
    let mut hash_map: HashMap<String, [String; 2]> = HashMap::new();
    for line in &lines[1..] {
        if line.len() == 0 {
            continue;
        }
        let key: String = line[0].replace(" ", "");
        let values = line[1]
            .replace("(", "")
            .replace(")", "")
            .replace(" ", "");
        let elements: Vec<String> = file_helper::split_string_by_separator(values, ",");
        hash_map.insert(key, [elements[0].clone(), elements[1].clone()]);
    }

    return hash_map;
}