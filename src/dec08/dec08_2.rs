use std::collections::{HashMap};
use crate::dec08::dec08_1::{get_input_lines,get_map};
use num::integer::{gcd};

pub fn run_code() {
    let lines: Vec<Vec<String>> = get_input_lines();
    let pattern: String = lines[0][0].clone();
    let hash_map = get_map(lines);
    let position_results = follow_map(pattern, hash_map);
    println!("{:?}", position_results);

    let mut result: u128 = position_results[0];
    for number in position_results[1..].iter() {
        let value = *number;
        result = (result * value) / gcd(result, value);
    }

    println!("🏆 Result: {:?}", result);
}

fn follow_single_position(pattern: &String, start_position: &String, map: &HashMap<String, [String; 2]>) -> u128
{
    let mut current_position: String = start_position.clone();
    let mut result = 0u128;
    let mut visited_positions: HashMap<String, u128> = HashMap::new();
    loop {
        for direction in pattern.chars() {
            let index = if direction == 'R' { 1usize } else { 0usize};
            result = result + 1;
            let values = map.get(&current_position).unwrap().clone();
            current_position = values[index].clone();

            if current_position.chars().nth(2).unwrap() == 'Z' {
                let identifier = current_position.clone();
                if visited_positions.contains_key(&identifier) {
                    return *visited_positions.get(&identifier).unwrap();
                }
                visited_positions.insert(identifier, result);

            }
        }
    }
}

fn follow_map(pattern: String, map: HashMap<String, [String; 2]>) -> Vec<u128> {
    let positions: Vec<String> = get_current_positions(&map);
    let mut position_results: Vec<u128> = Vec::new();
    for position in positions.iter() {
        position_results.push(follow_single_position(&pattern, position, &map))
    }

    return position_results;
}

fn get_current_positions(map: &HashMap<String, [String; 2]>) -> Vec<String> {
    let mut positions: Vec<String> = Vec::new();
    for (key, _) in map {
        if key.chars().nth(2).unwrap() == 'A' {positions.push(key.clone());}
    }
    return positions;
}